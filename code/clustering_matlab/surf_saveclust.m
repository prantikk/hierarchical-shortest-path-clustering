function [clustnss] = surf_saveclust(varargin)

if size(varargin,2)==0
    range=[2,3,4,5,6,7,8,9,10,12,15,20,25,50,75,100,150,200,250,300,350,400,450,500];
    %range=[250];
    rh_feats_fn = 'RH_feats_norm.1D';
    lh_feats_fn = 'LH_feats_norm.1D';
    linkage_fn = 'surf_linkage.1D';
    prefix = '';
else
    range = varargin{1};
    rh_feats_fn = varargin{2};
    lh_feats_fn = varargin{3};
    linkage_fn = varargin{4};
    prefix=varargin{5};
end

RH_feats_norm=importdata(rh_feats_fn);
LH_feats_norm=importdata(lh_feats_fn);
feats_norm = [RH_feats_norm(:,2:end); LH_feats_norm(:,2:end)];
linkage=load(linkage_fn);

top=max(range);

% figure out initial ordering with travelling salesman between mc clusters
mc = 10
tempc = cluster(linkage,'maxclust',mc);
initcc = zeros(mc,size(feats_norm,2));
for cc = 1:mc
    initcc(cc,:) = median(feats_norm(find(tempc==cc),:),1);
end
dmatr = squareform(pdist(initcc,'cityblock'));
paths = perms(1:mc);
plens = zeros(size(paths,1),1);
for ppii=1:size(paths)
   pp=paths(ppii,:);
   for ii=1:mc-1
      plens(ppii) = plens(ppii) + dmatr(pp(ii),pp(ii+1));
   end
end
[minp_y minp_i] = min(plens);
path_old = paths(minp_i,:);
prevc=tempc;
for newi=1:mc
    prevc(tempc==path_old(newi))=newi;
end

clustnss = prevc

for c=mc+1:top
    c
    nextc=zeros(size(prevc));
    tempc=cluster(linkage,'maxclust',c);
    % find what split
    breakpt=1;
    ind=find(prevc==breakpt);
    while length(unique(tempc(ind)))==1
        breakpt=breakpt+1;
        ind=find(prevc==breakpt);
    end
    % so now we know cluster breakpt split in previous one
    % anything to the right + 1 and keep values to left
    nextc(find(prevc>breakpt))=prevc(find(prevc>breakpt))+1;
    nextc(find(prevc<breakpt))=prevc(find(prevc<breakpt));
    
    % now it's prepared for next insertion, will determine which one is
    % closest to its neighbor in feature space (brik)
    ln=median(feats_norm(find(nextc==(breakpt-1)),:),1);
    rn=median(feats_norm(find(nextc==(breakpt+2)),:),1);
    a=unique(tempc(ind));
    nc1=median(feats_norm(find(tempc==a(1)),:),1);
    nc2=median(feats_norm(find(tempc==a(2)),:),1);
    
    % check distances
    if breakpt==1 % no left neighbors
        cmat = [nc1;nc2;ones(size(nc1))*10000;rn];
        dmatr=squareform(pdist(cmat,'cityblock'));
    elseif breakpt==(c-1) % no right neighbors"
        cmat = [nc1;nc2;ln;ones(size(nc1))*10000];
        dmatr=squareform(pdist(cmat,'cityblock'));
    else    
        cmat = [nc1;nc2;ln;rn];
        dmatr=squareform(pdist(cmat,'cityblock'));
    end
        
    if dmatr(1,3)<=dmatr(2,3) || dmatr(1,4)>=dmatr(2,4) 
    % that is, if first is closer to the left or farther from the right
        nextc(find(tempc==a(1)))=breakpt;
        nextc(find(tempc==a(2)))=breakpt+1;
    else
        nextc(find(tempc==a(1)))=breakpt+1;
        nextc(find(tempc==a(2)))=breakpt;
    end
    if find(range==c)
        fprintf('Saving at level %i',c);
        %Right hemisphere
        beg_ind = 1;
        end_ind = size(RH_feats_norm,1);
        cl_rh = [RH_feats_norm(:,1)  nextc(beg_ind:end_ind,:)];
        cl_rh_f = zeros(max(RH_feats_norm(:,1))+1,2);
        cl_rh_f(RH_feats_norm(:,1)+1,1) = cl_rh(:,1);
        cl_rh_f(RH_feats_norm(:,1)+1,2) = cl_rh(:,2);
        %Left hemisphere
        beg_ind = end_ind+1;
        end_ind = end_ind+size(LH_feats_norm,1);
        cl_lh = [LH_feats_norm(:,1)  nextc(beg_ind:end_ind,:)];
        cl_lh_f = zeros(max(LH_feats_norm(:,1))+1,2);
        cl_lh_f(LH_feats_norm(:,1)+1,1) = cl_lh(:,1);
        cl_lh_f(LH_feats_norm(:,1)+1,2) = cl_lh(:,2);
        %Both hemispheres
        cl_both = [[RH_feats_norm(:,1); LH_feats_norm(:,1)+500000] nextc];
        save(sprintf('%ssurf_cl%i.rh.1D.dset',prefix,c),'cl_rh_f','-ascii');
        save(sprintf('%ssurf_cl%i.lh.1D.dset',prefix,c),'cl_lh_f','-ascii');
        save(sprintf('%ssurf_cl%i.both.1D.dset',prefix,c),'cl_both','-ascii');
    end
    clustnss = [clustnss nextc];
    prevc=nextc;
    
end
