function [] = surf_singmap_match(varargin)

if length(varargin) > 0
    clust1fn=varargin{1};
    clust2fn=varargin{2};
end

cl1 = importdata(clust1fn);
cl2 = importdata(clust2fn);

cl1r = cl1(:,1);
cl2r = cl2(:,1);

mnodes=intersect(cl1r,cl2r);
r1rows = find(ismember(cl1r,mnodes)); %this working depends on whether cs are ordered. this should be sorted.
r2rows = find(ismember(cl2r,mnodes));

mv1 = cl1(r1rows,2);
mv2 = cl2(r2rows,2);
   
M = mv1~=0;
%compute cost matrix
nk = max(mv1);

costmat = -confusionmat(mv1,mv2);

%do hungarian algorithm

[mapping, cost] = munkres(costmat);

percost = (sum(M)+cost)/sum(M);
VI = varinfo(mv1,mv2);

%costmat = diag(costmat(1:nk,mapping));

out = zeros(length(M),1);
for i =1:nk
    out(mv2==mapping(i)) = i;
end

frange = 1:nk;
rrange = frange(end:-1:1);

indif = min(sum(abs(frange-mapping)), sum(abs(rrange-mapping)));

fprintf('%f %i\n',VI,indif );

end
