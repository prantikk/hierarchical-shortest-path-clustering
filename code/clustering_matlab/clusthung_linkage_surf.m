function [outmat]= clusthung_linkage_surf(varargin)

if length(varargin) > 0
    r1_masklev=varargin{1};
    r1_maskclust=varargin{2};
    r2_masklev=varargin{3};
    r2_maskclust=varargin{4};
else
    r1_masklev=2;
    r1_maskclust=0;
    r2_masklev=2;
    r2_maskclust=0;
end

addpath(genpath('/data/SFIM/matlab'));

link1fn='r1_linkage.1D';
link1_RH_feats_norm = 'r1_RH_feats_norm.1D';
link1_LH_feats_norm = 'r1_LH_feats_norm.1D';
link2fn='r2_linkage.1D';
link2_RH_feats_norm = 'r2_RH_feats_norm.1D';
link2_LH_feats_norm = 'r2_LH_feats_norm.1D';
l1 = importdata(link1fn);
l1_rhfn = importdata(link1_RH_feats_norm);
l1_lhfn = importdata(link1_LH_feats_norm);
l2 = importdata(link2fn);
l2_rhfn = importdata(link2_RH_feats_norm);
l2_lhfn = importdata(link2_LH_feats_norm);

range=[5,10,25,50:10:750,1000,1500];
%range=[500];
%range=[5,50,100];
%range=[1000];

cl1= cluster(l1,'maxclust',r1_masklev);
cl2= cluster(l2,'maxclust',r2_masklev);
cl1r = [l1_rhfn(:,1) ; l1_lhfn(:,1)+500000];
cl2r = [l2_rhfn(:,1) ; l2_lhfn(:,1)+500000];

mnodes=intersect(cl1r,cl2r);
r1rows = find(ismember(cl1r,mnodes)); %this working depends on whether c's are ordered. this should be sorted.
r2rows = find(ismember(cl2r,mnodes));

percosts = [];
VIs = [];
baseline = 2*log(range);


for clnum=range
    cl1= cluster(l1,'maxclust',clnum);
    cl2= cluster(l2,'maxclust',clnum);

    mv1 = cl1(r1rows,:);
    mv2 = cl2(r2rows,:);

    M = mv1~=0;
    %compute cost matrix
    nk = max(mv1);
    
    costmat = -confusionmat(mv1,mv2);
    
    %do hungarian algorithm
    
    [mapping, cost] = munkres(costmat);
    
    percost = (sum(M)+cost)/sum(M);
    VI = varinfo(mv1,mv2);

    percosts = [percosts percost];
    VIs = [VIs VI]
    
    costmat = diag(costmat(1:nk,mapping));

    out = zeros(length(M),1);
    for i =1:nk
        out(mv2==mapping(i)) = i;
    end
    
    rh_nodes = mnodes(mnodes<500000);
    rh_labs = mv1(mnodes<500000);
    cl_RH = [rh_nodes rh_labs];
    lh_nodes = mnodes(mnodes>500000)-500000;
    lh_labs = mv1(mnodes>500000);
    cl_LH = [lh_nodes lh_labs];
     
    cl_RH_f = zeros(max(link1_RH_feats_norm(:,1)+1),2);
    cl_RH_f(cl_RH(:,1)+1,1) = cl_RH(:,1);
    cl_RH_f(cl_RH(:,1)+1,2) = cl_RH(:,2);
    save(sprintf('cl%i_r1-r2.rh.1D.dset',clnum),'cl_RH_f','-ascii');
    cl_LH_f = zeros(max(link1_LH_feats_norm(:,1)+1),2);
    cl_LH_f(cl_LH(:,1)+1,1) = cl_LH(:,1);
    cl_LH_f(cl_LH(:,1)+1,2) = cl_LH(:,2);
    save(sprintf('cl%i_r1-r2.lh.1D.dset',clnum),'cl_LH_f','-ascii');
    
    rh_nodes = mnodes(mnodes<500000);
    rh_labs = out(mnodes<500000);
    cl_RH = [rh_nodes rh_labs];
    lh_nodes = mnodes(mnodes>500000)-500000;
    lh_labs = out(mnodes>500000);
    cl_LH = [lh_nodes lh_labs];
    
    %Save all nodes for MapIcosahedron
    cl_RH_f = zeros(max(link2_RH_feats_norm(:,1)+1),2);
    cl_RH_f(cl_RH(:,1)+1,1) = cl_RH(:,1);
    cl_RH_f(cl_RH(:,1)+1,2) = cl_RH(:,2);
    save(sprintf('cl%i_r2-r1.rh.1D.dset',clnum),'cl_RH','-ascii');
    cl_LH_f = zeros(max(link2_LH_feats_norm(:,1)+1),2);
    cl_LH_f(cl_LH(:,1)+1,1) = cl_LH(:,1);
    cl_LH_f(cl_LH(:,1)+1,2) = cl_LH(:,2);
    save(sprintf('cl%i_r2-r1.lh.1D.dset',clnum),'cl_LH','-ascii');

end

outmat = [range;VIs;percosts]'

N = length(mnodes);
n = 2;
ks = range;
nk = length(ks);
VIs = zeros(n,length(ks));
muVI = zeros(length(ks),1);
for k = 1:nk
    display(ks(k));
    r1 = floor(ks(k)*rand(N,n)+1);
    r2 = floor(ks(k)*rand(N,n)+1);
    for i = 1:n 
        VIs(i,k) = varinfo(r1(:,i),r2(:,i));
    end
end
muVI  = mean(VIs,1);

outmat = [outmat muVI'];
save('r1_v_r2_VI.txt','outmat','-ASCII');

end