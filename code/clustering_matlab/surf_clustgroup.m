function [] = surf_clustgroup(varargin)
    RHs = cell(4,1);
    LHs = cell(4,1);

    RH_group_feats = [];
    LH_group_feats = [];

    outlabel = varargin{1};
    outdir = sprintf('surfclustgroup_%s',outlabel);
    system(sprintf('mkdir %s',outdir));
    
    start = 2;
    dirs = varargin;
    
    if ~exist(sprintf('%s/RH_feats_norm.1D',outdir)) && ~exist(sprintf('%s/LH_feats_norm.1D',outdir))
        for sub=start:length(dirs)
            dirs{sub}
            if exist(sprintf('%s/RH_feats_norm.1D',dirs{sub}))
                RHs{sub}=load(sprintf('%s/RH_feats_norm.1D',dirs{sub}));
                LHs{sub}=load(sprintf('%s/LH_feats_norm.1D',dirs{sub}));
                if sub==start
                    RH_com_nodes = RHs{sub}(:,1);
                    LH_com_nodes = LHs{sub}(:,1);
                else 
                    RH_com_nodes=intersect(RH_com_nodes,RHs{sub}(:,1) );
                    LH_com_nodes=intersect(LH_com_nodes,LHs{sub}(:,1) );
                end
            else
                fprintf('Cant find feats for %s',dirs{sub});
            end
        end
        for sub=start:length(dirs)
            RH_inds = find(ismember(RHs{sub}(:,1),RH_com_nodes));
            LH_inds = find(ismember(LHs{sub}(:,1),LH_com_nodes));
            RH_group_feats = [RH_group_feats RHs{sub}(RH_inds,2:end)];
            LH_group_feats = [LH_group_feats LHs{sub}(LH_inds,2:end)];
        end
        RH_feats_out  = [RH_com_nodes RH_group_feats];
        save(sprintf('%s/RH_feats_norm.1D',outdir),'RH_feats_out');
        LH_feats_out  = [LH_com_nodes LH_group_feats];
        save(sprintf('%s/LH_feats_norm.1D',outdir),'LH_feats_out');
    else
        RH_feats_out=importdata(sprintf('%s/RH_feats_norm.1D',outdir));
        LH_feats_out=importdata(sprintf('%s/LH_feats_norm.1D',outdir));
        RH_com_nodes = RH_feats_out(:,1);
        RH_group_feats = RH_feats_out(:,2:end);
        LH_com_nodes = LH_feats_out(:,1);
        LH_group_feats = LH_feats_out(:,2:end);  
    end
    group_feats = [RH_group_feats;LH_group_feats];
    l = linkage(group_feats,'ward','euclidean');
    save(sprintf('%s/surf_linkage.1D',outdir),'l','-ascii');
    chdir(outdir);
    surf_saveclust();
end
