from numpy import * 
from numpy.linalg import svd,norm,lstsq
import numpy as np
import os
import nibabel as nib
from sys import argv,exit
import scipy.stats as stats

#import ipdb

def andb(arrs):
	result = np.zeros(arrs[0].shape)
	for aa in arrs: result+=np.array(aa,dtype=np.int)
	return result

def niwrite(data,affine, name , header):
	thishead = header.copy()
	thishead.set_data_shape(list(data.shape))
	outni = nib.Nifti1Image(data,affine,header=thishead)
	outni.to_filename(name)
	print 'done.'

def readclist(fn):
	clist = []
	try:
		clist = [int(ii) for ii in np.loadtxt(fn)]
	except:
		clines_j = ','.join([ll.strip() for ll in open(fn).readlines()] )
		clist = [int(ii) for ii in clines_j.split(',')]
	return clist

def getfeats(tsdo,mix,mixacc,dmix=None):
	#Mean center, denoise
	tsd = (tsdo.T-tsdo.T.mean(0)).T
	lss = lstsq(mix,tsd.T)[0]
	#ipdb.set_trace()
	tsd = np.dot(lss.T[:,mixacc],mix[:,mixacc].T)
	tsd = (tsd.T-tsd.T.mean(0)).T #good
	###tsd = tsd-tsd.mean(0) #BAD
	tsdo = tsd.copy()
	if dmix != None: #If a design matrix is provided (i.e. as from sparse PCA)
		#Detrend
		if not os.path.exists('pol4.1D'): os.system("3dDeconvolve -nodata %i 1 -polort 4 -x1D_stop -x1D stdout: | 1dcat stdin: > pol4.1D" % 	(tsd.shape[1]))
		pol4 = loadtxt('pol4.1D')
		drifts = lstsq(pol4,tsd.T)
		tsd = tsd - drifts[0].T.dot(pol4.T)
		tsdo = tsd.copy()
		#Fit design matrix
		tsd_z = ((tsd.T-tsd.T.mean(0))/tsd.T.std(0)).T
		lss = np.linalg.lstsq(dmix,tsd_z.T)[0]
	else: #If making features just from mixing matrix
		#Fit mixing matrix to denoised unit variance data
		tsd_z = ((tsd.T-tsd.T.mean(0))/tsd.T.std(0)).T
		lss = np.linalg.lstsq(mix[:,mixacc],tsd_z.T)[0]
	#Threshold maps
	r_pos_max = stats.scoreatpercentile(lss[lss>0],99.7)
	r_neg_max = stats.scoreatpercentile(lss[lss<0],0.3)
	sU_r = lss.T.copy()
	lss[lss>0.999]=r_pos_max #Should try r_pos_max
	lss[lss<-0.999]=r_neg_max #Should try r_neg_max
	sU_rz = np.arctanh(lss.T)
	sU_rz_mean = sU_rz.mean()
	sU_rz_std = sU_rz.std()
	#ipdb.set_trace()
	sU_rz /= sU_rz_std
	sU_rz[sU_rz<0] = 0 #Don't null to keep negative values
	return sU_rz,sU_r

def loadmix(mixfn,mixaccfn=None):
	#Load & parse time series and component # listings
	mix_z = np.loadtxt(mixfn)
	mix_z = (mix_z-mix_z.mean(0))/mix_z.std(0)
	if mixaccfn!=None: return mix_z, readclist(mixaccfn)
	else: return mix_z,None

def makevolmask(maskin):
	if type(maskin)==str: 
		maskv = nib.load(maskin)
		mask = maskv.get_data()
	else: mask=maskin	
	sh3d = mask.shape[0:3]
	mask1d = mask.reshape([np.prod(sh3d)])!=0
	ind1d = np.arange(np.prod(sh3d))[mask1d]
	return mask1d,ind1d

def loadvolume(volfn,maskfn=None):
	#Load volume time series
	tsv = nib.load(volfn)
	outpr = volfn.split('/')[-1].split('.nii')[0]
	tsd = tsv.get_data()
	sh4d = tsd.shape
	tsd = tsd.reshape([np.prod(sh4d[0:3]),sh4d[3]])
	if maskfn!=None: mask1d,ind1d = makevolmask(maskfn)
	else: mask1d,ind1d = makevolmask(tsd[:,0])
	tsdo= np.array(tsd[mask1d,:],dtype=np.float64)
	return tsdo,sh4d,mask1d,ind1d,tsv,outpr

def loadsurface(surffn):
	tsd = np.loadtxt(surffn)
	outpr = surffn.split('/')[-1].split('.1')[0]
	sh2d = tsd.shape
	mask1d = np.max(tsd[:,1:],axis=1)!=0
	ind1d = tsd[mask1d,0]
	tsdo= np.array(tsd[mask1d,1:],dtype=np.float64)
	return tsdo,sh2d,mask1d,ind1d,outpr

def savevolume(sU_rz,sh4d,mask1d,volref,prefix,part_mask1d=None):
	#Save volume data
	sU_r = np.zeros([sh4d[0]*sh4d[1]*sh4d[2],sU_rz.shape[1]])
	sU_r[mask1d,:] = sU_rz
	if part_mask1d!=None: 
		sU_r[part_mask1d==False,:]=np.zeros([(part_mask1d==False).sum(),sU_rz.shape[1]])
	sU_r = sU_r.reshape([sh4d[0],sh4d[1],sh4d[2],sU_rz.shape[1]])
	niwrite(sU_r,volref.get_affine(),'%s_2c.nii' % prefix,volref.get_header())

def savesurface(sU_rz,ind1d,prefix,part_ind1d=None):
	sU_r = np.zeros([ind1d.shape[0],sU_rz.shape[1]+1])
	sU_r[:,0] = ind1d	
	sU_r[:,1:] = sU_rz
	if part_ind1d!=None:
		membership=np.array([nn in part_ind1d for nn in ind1d])
		sU_r = sU_r[membership,:]
	np.savetxt('%s_2c.1D.dset' % (prefix),sU_r)
	
def checkempty(fsets,div):
	if type(fsets)==list:
		allf = np.vstack(fsets)
	else: allf = fsets
	if div == 0: return np.arange(allf.shape[1])
	fracs = (allf>2).sum(axis=0)/(allf>0).sum(axis=0,dtype=np.float)
	sel = fracs>(np.median(fracs)/div)
	return np.arange(allf.shape[1])[sel].tolist()
	
from optparse import OptionParser
parser=OptionParser()
parser.add_option('',"--vol",dest='vol',help="Volume data",default='')
parser.add_option('',"--vol1Dmask",dest='vol1Dmask',help="Mask for writing 1D features",default='')
parser.add_option('',"--lsurf",dest='lsurf',help="Left hemi surface data",default='')
parser.add_option('',"--rsurf",dest='rsurf',help="Right hemi surface data",default='')
parser.add_option('',"--mix",dest='mix',help="Denoising mixing matrix",default='')
parser.add_option('',"--mixacc",dest='mixacc',help="Accepted --mix components",default='')
parser.add_option('',"--dmix",dest='dmix',help="Design matrix",default='')
parser.add_option('',"--noempty",dest='noempty',help="Divisor for median fraction of non-zero Z values over p>0.05 used to determine an empty component, default 0",default='0')
(options,args) = parser.parse_args()

#Load mix and dmix
mix_z,mixacc = loadmix(options.mix,options.mixacc)
if options.dmix!='': dmix_z,dmixacc = loadmix(options.dmix)
else: dmix_z = None

#import ipdb

#Load and compute features
#tsdo,vol_sh4d,volfull_mask1d,volfull_ind1d,volref,volpre = loadvolume(options.vol)
#vol_sU_rz,vol_sU_r = getfeats(tsdo,mix_z,mixacc,dmix_z)
tsdo,lh_sh2d,lh_mask1d,lh_ind1d,lhpre = loadsurface(options.lsurf)
lh_sU_rz,lh_sU_r = getfeats(tsdo,mix_z,mixacc,dmix_z)
tsdo,rh_sh2d,rh_mask1d,rh_ind1d,rhpre = loadsurface(options.rsurf)
rh_sU_rz,rh_sU_r = getfeats(tsdo,mix_z,mixacc,dmix_z)

#Select features and save
#ne = checkempty([vol_sU_rz,lh_sU_rz,rh_sU_rz],float(options.noempty))
#savevolume(vol_sU_rz[:,ne],vol_sh4d,volfull_mask1d,volref,volpre )
#if options.vol1Dmask!='': 
#	volpart_mask1d,volpart_ind1d = makevolmask(options.vol1Dmask)
#	savevolume(vol_sU_rz[:,ne],vol_sh4d,volfull_mask1d,volref,'_'.join([volpre,'masked']),part_mask1d=volpart_mask1d)
#	savesurface(vol_sU_rz[:,ne],volfull_ind1d,'_'.join([volpre,'masked']),part_ind1d=volpart_ind1d)
ne = np.arange(mix_z.shape[1]) #Omitting volume-based stuff
savesurface(lh_sU_rz[:,ne],lh_ind1d,lhpre)
savesurface(rh_sU_rz[:,ne],rh_ind1d,rhpre)
if options.dmix=='': 
	savevolume(vol_sU_r[:,ne],vol_sh4d,volfull_mask1d,volref,volpre+'_corr' )
	savesurface(lh_sU_r[:,ne],lh_ind1d,lhpre+'_corr')
	savesurface(rh_sU_r[:,ne],rh_ind1d,rhpre+'_corr')


