sumadir=$1
sub=$2
Necho=$3
expanat=$4

thispwd=`pwd`

echos=`seq 1 $Necho`
hemis="rh lh"

rm *1D.dset*


3dcopy -overwrite ${sumadir}/brainmask.nii ${sumadir}/brainmask
if [ ! -e brainmask_als+orig.HEAD ]; then  
ln -s $expanat .
anatbase=${expanat##*/}
echo "@SUMA_AlignToExperiment -wd -surf_anat ${sumadir}/brainmask+orig -exp_anat ${anatbase} -prefix ./brainmask_als"
@SUMA_AlignToExperiment -wd -surf_anat ${sumadir}/brainmask+orig -exp_anat ${anatbase} -prefix ./brainmask_als
fi

set -e

3dresample -overwrite -dxyz 2 2 2 -prefix brainmask_als_rg -inset brainmask_als+orig
3dAutobox -overwrite -prefix brainmask_als_rg  brainmask_als_rg+orig
3daxialize -overwrite -prefix brainmask_als_rg brainmask_als_rg+orig

#Defaults
nvals_vr=`3dnvals e1_vr.nii.gz`
nvals_in=`3dnvals e1_in.nii.gz`
dummies=`ccalc -i ${nvals_vr}-${nvals_in}`
startvol=$dummies
endvol='$'

for echon in $echos
do

3dTstat -overwrite -prefix ./e${echon}_vr_mn.nii.gz ./e${echon}_vr.nii.gz[${startvol}..${endvol}]
3dTcat -overwrite -prefix ./e${echon}_vr_bp.nii.gz ./e${echon}_vr.nii.gz[${startvol}..${endvol}]
3dcalc -overwrite -a ./e${echon}_vr_bp.nii.gz -b ./e${echon}_vr_mn.nii.gz -expr 'a+b' -prefix ./e${echon}_vr_bp.nii.gz

done

#Prepare loose mask
3dAutomask -peels 1 -dilate 3 -prefix ocmask.nii.gz -overwrite e1_vr.nii.gz 
3dresample -overwrite -master eBvrmask.nii.gz -inset ocmask.nii.gz -prefix ocmask.nii.gz

for hemi in $hemis
do
cd $sumadir
MapIcosahedron -overwrite  -spec ${sub}_${hemi}.spec -ld 60 -prefix ld60_
cd $thispwd


for echon in $echos
do

3dVol2Surf -overwrite -cmask "-a ocmask.nii.gz -b ./e${echon}_vr_bp.nii.gz -expr b*notzero(a)" -spec ${sumadir}/ld60_${sub}_${hemi}.spec -surf_A smoothwm -surf_B pial -sv ./brainmask_als+orig. -grid_parent ./e${echon}_vr_bp.nii.gz -map_func ave -f_steps 15 -f_index nodes -outcols_NSD_format -out_niml ./e${echon}_vr_bp.${hemi}.niml.dset

if [ "$echon" == "1" ]; then 3dBrickStat -count ./e${echon}_vr_bp.${hemi}.niml.dset[0] > ${hemi}_nct; fi

padval=`3dBrickStat -mean ./e${echon}_vr_bp.${hemi}.niml.dset`

ConvertDset -o_niml_bi -input e${echon}_vr_bp.${hemi}.niml.dset -pad_to_node ld60 -overwrite -prefix e${echon}_vr_bp.${hemi}

3dcalc -a e${echon}_vr_bp.${hemi}.niml.dset -expr "a+equals(a,0)*${padval}" -overwrite -prefix  e${echon}_vr_bp.${hemi}.niml.dset

SurfSmooth -overwrite  -input e${echon}_vr_bp.${hemi}.niml.dset -target_fwhm 12mm -spec ${sumadir}/ld60_${sub}_${hemi}.spec -surf_A pial -surf_B smoothwm -met HEAT_07 -add_index -output ./e${echon}_vr_bp_sm.${hemi}.1D.dset

done
done
