import glob
from optparse import OptionParser
import numpy as np
import os
import ipdb

parser=OptionParser()
parser.add_option('',"--search",dest='search',help="Search string for surfclustgroup directories",default='')
parser.add_option('',"--mode",dest='mode',help="0=prepare script; 1=read results", default='0')
parser.add_option('',"--jobs",dest='jobs',help="Number of CPUs to use", default=2)
parser.add_option('',"--dirpre",dest='dirpre',help="Directory to write results to", default='scg_cross_test')

(options,args) = parser.parse_args()


seldirs = glob.glob(options.search)
sa = np.argsort([int(ss.split('_')[-1]) for ss in seldirs ]).tolist()
seldirs = [seldirs[ii] for ii in sa]
dicemat = np.zeros([len(seldirs),len(seldirs)])
idifmat = np.zeros([len(seldirs),len(seldirs)])
#Set MATLAB call here
mlcmd = '/Applications/MATLAB_R2012b.app/bin/matlab'
scpath = '../clustering_matlab/'

if options.mode=='0':
	sl = []
	try: 
		os.system('mkdir %s' % (options.dirpre))
	except:
		pass
	for ii in range(len(seldirs)):
		for jj in range(len(seldirs)):
			if ii!=jj:
				if os.path.exists( "%s/surf_cl250.both.1D.dset" % (seldirs[ii]) ) and os.path.exists( "%s/surf_cl250.both.1D.dset" % (seldirs[jj]) ) and not os.path.exists('%s/%s_%s' % (options.dirpre,seldirs[ii],seldirs[jj]) ) :
					print 'yes'
					sl.append("%s -nodisplay -r \"addpath('%s'); surf_singmap_match('%s/surf_cl250.both.1D.dset','%s/surf_cl250.both.1D.dset');exit\" > %s/%s_%s" % (mlcmd, scpath, seldirs[ii],seldirs[jj],options.dirpre,seldirs[ii],seldirs[jj]))
	ofh = open('dirpre.cmds','w')
	ofh.write('\n'.join(sl))
	ofh.close()

else:
	for ii in range(len(seldirs)):
		for jj in range(ii+1,len(seldirs)):
			if os.path.exists( "%s/surf_cl250.both.1D.dset" % (seldirs[ii]) ) and os.path.exists( "%s/surf_cl250.both.1D.dset" % (seldirs[ii]) ) :
				try:
					print ii,jj
					arr = [float(vv) for vv in open('%s/%s_%s' % (options.dirpre,seldirs[ii],seldirs[jj])).readlines()[-1].split()]
					dicemat[ii,jj] = arr[0]
					idifmat[ii,jj] = arr[1]
					dicemat[jj,ii] = arr[0]
					idifmat[jj,ii] = arr[1]
				except:
					import ipdb
					ipdb.set_trace()
			else:
				import ipdb
				ipdb.set_trace()

	"Sort VI matrix by order of stability"
	new_ind = dicemat.sum(-1).argsort()
	dicemat2 = np.zeros([int(len(seldirs)),int(len(seldirs))])
	for jj in range(len(dicemat2)):
		for kk in range(len(dicemat2)):
			dicemat2[jj,kk] = dicemat[new_ind[jj],new_ind[kk]]
	
	imshow(dicemat2,vmin=dicemat2[dicemat2!=0].min(),vmax=dicemat2.max(),interpolation='nearest')

	new_ind_sel = new_ind[:len(new_ind)/2]
	idifmat2=np.zeros([len(new_ind_sel),len(new_ind_sel)])
	for jj_ii in range(len(new_ind_sel)):
		for kk_ii in range(len(new_ind_sel)):
			jj = new_ind_sel[jj_ii]
			kk = new_ind_sel[kk_ii]
			idifmat2[jj_ii,kk_ii] = idifmat[jj,kk]

	new_sum_ind_sel = idifmat2.sum(1).argsort()
	idifmat3 = np.zeros([len(new_sum_ind_sel),len(new_sum_ind_sel)])
	for jj_ii in range(len(new_sum_ind_sel)):
		for kk_ii in range(len(new_sum_ind_sel)):
			jj = new_sum_ind_sel[jj_ii]
			kk = new_sum_ind_sel[kk_ii]
			idifmat3[jj_ii,kk_ii] = idifmat2[jj,kk]

	print "Optimal clustering is in %s" % seldirs[new_ind[idifmat2.sum(1).argsort().argmin()]]

