import glob
from numpy.random import permutation
import os
import sys
from math import factorial

"Permutation combination stuff from stack overflow"

def product(iterable):
    prod = 1
    for n in iterable:
        prod *= n
    return prod

def npr(n, r):
    """
    Calculate the number of ordered permutations of r items taken from a
    population of size n.

    >>> npr(3, 2)
    6
    >>> npr(100, 20)
    1303995018204712451095685346159820800000
    """
    assert 0 <= r <= n
    return product(range(n - r + 1, n + 1))

def ncr(n, r):
    """
    Calculate the number of unordered combinations of r items taken from a
    population of size n.

    >>> ncr(3, 2)
    3
    >>> ncr(100, 20)
    535983370403809682970
    >>> ncr(100000, 1000) == ncr(100000, 99000)
    True
    """
    assert 0 <= r <= n
    if r > n // 2:
        r = n - r
    return npr(n, r) // factorial(r)


from optparse import OptionParser
parser=OptionParser()
parser.add_option('',"--search",dest='search',help="Search string for features",default='')
parser.add_option('',"--subsizes",dest='subsizes',help="Groupsize for subsampling. Ex: '-1' for LOO, '0.5' for test-retest, '20' for arbitrary power analysis; '-3,0.1,19' for a set",default='')
parser.add_option('',"--reps",dest='reps',help="Max number of permutations to test. Ex: '10' or '10,5,50' for set of subsizes",default='')
parser.add_option('',"--setname",dest='setname',help="Name of set",default='group')
parser.add_option('',"--modes",dest='modes',help="Methods to test: default spca,meica",default='spca,meica')
parser.add_option('',"--test",dest='test',action='store_true',help="Test mode, don't check directory existence",default=False)
(options,args) = parser.parse_args()

#Initialize
modes=options.modes.split(',')
mode_cmd_dict=dict()
mode_prep_dict=dict()
for mode in modes:
	mode_cmd_dict[mode]=[]
	mode_prep_dict[mode]=[]
samplings = []

#Set MATLAB call here
matcmd = "matlab -nodisplay -r \"surf_clustgroup"
prepcmd = "matlab -nodisplay -r \"surf_groupfeats"

#Search for directories
dirs_in = [ll.rstrip() for ll in os.popen("find . -name '%s' " % options.search).readlines()]

#Check for workable directories
expected=len(modes)
actual=0
dirs = []
for dd in dirs_in:
	actual=0
	if not options.test:	
		for mode in modes:
			if os.path.exists('%s/%s_feats/RH_feats_norm.1D' % (dd,mode)): actual+=1
		if actual==expected: dirs.append(dd)
	else: dirs.append(dd) 

#Set subgroup sizes and reps
Ndirs = len(dirs)
in_sample_sizes = [float (ss) for ss in options.subsizes.split(',')]
in_reps = [int(rr) for rr in options.reps.split(',')]
sample_sizes = []
reps = []
for ss in in_sample_sizes:
	if ss<0: sample_sizes.append(Ndirs-abs(int(ss)))
	elif ss > 0 and ss < 1: sample_sizes.append(int(round(Ndirs*float(ss))))
	elif ss >= 1 : sample_sizes.append(int(ss))
for ss_ii in range(len(sample_sizes)):
	if len(in_reps)==1: ss_ii = 0
	reps.append(min([ncr(Ndirs,sample_sizes[ss_ii]),in_reps[ss_ii]]))

#Loop over sample sizes and reps
for sample_size in sample_sizes:
	for rep in reps:
		for ii in range(rep):
			per_dirs = permutation(sorted(dirs))
			pp = sorted(per_dirs[:sample_size])
			while pp in samplings:
				per_dirs = permutation(sorted(dirs))
				pp = sorted(per_dirs[:sample_size])
			print 'leaving out ', set(per_dirs).difference(pp)
			samplings.append(pp)
			for mode in modes:
				cmd = "%s('%s_%s_%i'," % (matcmd,mode,'_'.join([options.setname,'g%i' % sample_size]),ii+1) + "\'" + "\',\'".join(['/'.join([dd,'%s_feats' % mode]) for dd in pp]) + "\'" + ")\""
				mode_cmd_dict[mode].append(cmd)
				cmd = "%s('%s_%s_%i'," % (prepcmd,mode,'_'.join([options.setname,'g%i' % sample_size]),ii+1) + "\'" + "\',\'".join(['/'.join([dd,'%s_feats' % mode]) for dd in pp]) + "\'" + ")\""
				mode_prep_dict[mode].append(cmd)

#Save different lists for different modes
for mode in modes:
	ofh = open('_'.join(['_permute_%s',options.setname]) % mode,'w')
	ofh.write('\n'.join(mode_cmd_dict[mode]))
	ofh.close()
	ofh = open('_'.join(['_prep_%s',options.setname]) % mode,'w')
	ofh.write('\n'.join(mode_prep_dict[mode]))
	ofh.close()

